import java.awt.Graphics;
import javax.swing.JPanel;

public class Shapes extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int choice; // user chooses shape
	
	//constructor sets the user's choice
	public Shapes(int userChoice)
	{
		choice = userChoice;
	}
	
	//draws a cascade of shapes starting from the top left corner
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		for (int i=0; i<=10;i++)
		{
			//pick the shape base on the user's choice
			switch (choice)
			{
				case 1: //draw rectangle
					g.drawRect(10+i*10, 10+i*10, 50+i*10, 50+i*10);
					break;
				case 2: //draw Oval
					g.drawOval(10+i*10, 10+i*10, 50+i*10, 50+i*10);
					break;
			}//end switch
		}// end for loop 
	}//end method paintComponent
}// end class
